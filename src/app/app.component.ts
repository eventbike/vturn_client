import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Client} from './supply/client';
import {ClientService} from './supply/services/client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ClientService]
})
export class AppComponent implements OnInit {

  public client!: Client;

  title = 'vTurn';

  constructor(public clientService: ClientService) {
    clientService.getClient().subscribe((res) => {
      this.client = res;
    });
  }

  ngOnInit(): void {
    this.clientService.registerClient();
  }

}
