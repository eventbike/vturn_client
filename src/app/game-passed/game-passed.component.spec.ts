import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamePassedComponent } from './game-passed.component';

describe('GamePassedComponent', () => {
  let component: GamePassedComponent;
  let fixture: ComponentFixture<GamePassedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamePassedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamePassedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
