import { Component, OnInit } from '@angular/core';
import {Game, GameStatus} from '../supply/game';
import {getDateFormattedString, getLiveGameTime} from '../supply/helpers/time';

@Component({
  selector: 'app-game-passed',
  templateUrl: './game-passed.component.html',
  styleUrls: ['./game-passed.component.scss']
})
export class GamePassedComponent implements OnInit {


  GAMES: Game[] = [
    { id: 1,
      date: new Date(),
      home: 'Jonas',
      guest: 'Martin',
      liveGame: {
        status: GameStatus.CANCELED,
        time: 600,
        scoreHome: 3,
        scoreGuest: 2,
        getTime(): string {
          return getLiveGameTime(this);
        }
      },
      getClock(): string[] {
        return getDateFormattedString(this.date);
      }
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  loadMore(): void {

  }
}
