import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Game, GameStatus, LiveGame} from '../supply/game';
import {getDateFormattedString, getLiveGameTime} from '../supply/helpers/time';

@Component({
  selector: 'app-game-upcoming',
  templateUrl: './game-upcoming.component.html',
  styleUrls: ['./game-upcoming.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GameUpcomingComponent implements OnInit {

  constructor() {
  }

  GAMES: Game[] = [
    { id: 1,
      date: new Date(Date.now() + 1200000),
      home: 'Jonas',
      guest: 'Hannes',
      liveGame: {
        status: GameStatus.UPCOMING,
        time: 600,
        scoreHome: 0,
        scoreGuest: 0,
        getTime(): string {
          return getLiveGameTime(this);
        }
      },
      getClock(): string[] {
        return getDateFormattedString(this.date);
      }
    }
  ];

  ngOnInit(): void {
    this.verify(this.GAMES);
  }

  verify(games: Game[]): void {
    for (const game of games) {
      const status = game.liveGame.status;
      if (status === GameStatus.DEBUG || status === GameStatus.FINISHED) {
        games.forEach((element1, index) => {
          if (element1 === game) { games.splice(index, 1); }
        });
      }
      else if (status === GameStatus.CANCELED) {
        if (game.date <= new Date()) {
          games.forEach((element2, index) => {
            if (element2 === game) { games.splice(index, 1); }
          });
        }
      }
    }
  }

  getButton(game: LiveGame): string[] {
    switch (game.status) {
      case GameStatus.CANCELED:
        return ['btn-danger disabled', 'Abgesagt'];
      case GameStatus.RUNNING:
        return ['btn-success', 'Live (' + game.getTime() + ')'];
      case GameStatus.UPCOMING:
        return ['btn-warning', 'Startet bald'];
      default:
        return ['btn-secondary disabled', 'debug'];
    }
  }

  loadMore(): void {

  }
}
