import { Component, OnInit } from '@angular/core';
import {Group} from '../supply/group';

@Component({
  selector: 'app-group-overview',
  templateUrl: './group-overview.component.html',
  styleUrls: ['./group-overview.component.scss']
})
export class GroupOverviewComponent implements OnInit {

  GROUPS: Group[] = [
    {
      id: 0,
      name: 'Gruppe A',
      teams: [
        {
          id: 0,
          name: 'Die Ottifanten',
          shortName: 'OTI',
          points: 9
        },
        {
          id: 1,
          name: 'Die grünen Tannbäumchen',
          shortName: 'DGT',
          points: 3
        },
        {
          id: 2,
          name: 'Borkwalder Baumkinder',
          shortName: 'BBK',
          points: 0
        },
        {
          id: 3,
          name: 'Gegen Gegen Gegen',
          shortName: 'GGG',
          points: 0
        }
      ]
    },
    {
      id: 0,
      name: 'Gruppe B',
      teams: [
        {
          id: 0,
          name: 'Affengeile Affen II',
          shortName: 'AGA',
          points: 9
        },
        {
          id: 1,
          name: 'Ohligs Schläger',
          shortName: 'OLS',
          points: 3
        },
        {
          id: 2,
          name: 'Die woodys',
          shortName: 'WOO',
          points: 0
        },
        {
          id: 3,
          name: 'Les Gocles',
          shortName: 'LES',
          points: 0
        }
      ]
    },
    {
      id: 0,
      name: 'Gruppe C',
      teams: [
        {
          id: 0,
          name: 'Weihnachtsbären',
          shortName: 'WBÄ',
          points: 9
        },
        {
          id: 1,
          name: 'Hoeneß Finanzberater',
          shortName: 'HFB',
          points: 3
        },
        {
          id: 2,
          name: 'Die katholische Kirche',
          shortName: 'DKK',
          points: 0
        }
      ]
    },
    {
      id: 0,
      name: 'Gruppe D',
      teams: [
        {
          id: 0,
          name: 'Die Schonneks der 5L',
          shortName: 'S5L',
          points: 9
        },
        {
          id: 1,
          name: 'TheKeck\'s',
          shortName: 'KEK',
          points: 3
        },
        {
          id: 2,
          name: 'Die Baumwollbolzer',
          shortName: 'BWB',
          points: 0
        }
      ]
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  getClass(length: number): string {
    const classes: string[] = [
      'col-sm-12 col-lg-12 col-xxl-12',
      'col-sm-12 col-lg-6 col-xxl-6',
      'col-sm-12 col-lg-6 col-xxl-4',
      'col-sm-12 col-lg-6 col-xxl-6',
      'col-sm-12 col-lg-6 col-xxl-4'
    ];
    if (length < 5) {
      return classes[length];
    }
    else {
      return classes[4];
    }
  }

}
