export interface Client {
  id: number;
  name: string;
  key: string;
  mode: number;
}
