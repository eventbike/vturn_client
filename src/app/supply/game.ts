export interface LiveGame {
  status: GameStatus;
  time: number;
  scoreHome: number;
  scoreGuest: number;
  getTime(): string;
}

export interface Game {
  id: number;
  date: Date;
  home: string;
  guest: string;
  liveGame: LiveGame;
  getClock(): string[];
}

export enum GameStatus {
  DEBUG, UPCOMING, RUNNING, FINISHED, CANCELED
}
