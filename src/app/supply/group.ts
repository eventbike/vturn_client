export interface Group {
  id: number;
  name: string;
  teams: Team[];
}

export interface Team {
  id: number;
  name: string;
  shortName: string;
  points: number;
}
