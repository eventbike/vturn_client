import { Injectable } from '@angular/core';
import {Client} from '../client';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Observable, of} from 'rxjs';

@Injectable()
export class ClientService {

  private client: Client = {
    id: -1,
    name: 'ng_debug',
    key: 'debug',
    mode: -1
  };
  private url = 'https://vturn-dev.eventbikezero.de/api/client/register';
  private body: string;

  public getClient(): Observable<Client> {
    return of(this.client);
  }

  constructor(private http: HttpClient, private cookieService: CookieService) {
    if (cookieService.check('a6f2ee42')) {
      const cookieData = cookieService.get('a6f2ee42');
      if (cookieData.length === 16) {
        this.client.key = cookieData;
        try {
          this.client.name = cookieService.get('a6f2ee52');
          this.client.id = Number(cookieService.get('a6f2ea42'));
          this.client.mode = Number(cookieService.get('a6f2ea52'));
        } catch (e) {
          // todo: error handling needed
        }
        this.body = 'data={}';
      }
      else {
        this.body = 'data={"name":"' + this.client.name + '"}';
      }
    }
    else {
      this.body = 'data={"name":"' + this.client.name + '"}';
    }
  }


  public registerClient(): void {
    if (this.client.key === 'debug') {
      this.http.post<Client>(this.url, this.body).subscribe(res => {
          this.client = res;
          this.postRegister();
        }, error => {
          console.log('Error: ' + error);
          this.registerFail();
        }
      );
    }
    else {
      this.postRegister();
    }
  }
  private postRegister(): void {
    const expire = new Date();
    expire.setDate(expire.getDate() + 1);
    this.cookieService.set('a6f2ee42', this.client.key, expire);
    this.cookieService.set('a7f7ee52', this.client.name, expire);
    this.cookieService.set('a6f2ea42', this.client.id.toString(), expire);
    this.cookieService.set('a7f7ea52', this.client.mode.toString(), expire);
  }
  private registerFail(): void {
    console.log('error');
  }
}
